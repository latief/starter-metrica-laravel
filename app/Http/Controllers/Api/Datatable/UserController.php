<?php

namespace App\Http\Controllers\Api\Datatable;

use App\User;
use Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Usercontroller extends Controller
{
    public function index(Request $request)
    {
        $user = User::get();

        return Datatables::of($user)->make(true);
    }
}
