<!-- jQuery  -->
<script src="{{URL::asset('metrica-assets/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('metrica-assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{URL::asset('metrica-assets/js/metisMenu.min.js')}}"></script>
<script src="{{URL::asset('metrica-assets/js/waves.min.js')}}"></script>
<script src="{{URL::asset('metrica-assets/js/jquery.slimscroll.min.js')}}"></script>

@yield('script')

<!-- App js -->
<script src="{{URL::asset('metrica-assets/js/app.js')}}"></script>

@yield('script-bottom')