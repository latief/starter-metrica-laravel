<!-- App favicon -->
<link rel="shortcut icon" href="{{ URL::asset('metrica-assets/images/favicon.ico') }}">
        
@yield('css')

<!-- App css -->
<link href="{{URL::asset('metrica-assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('metrica-assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('metrica-assets/css/metisMenu.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('metrica-assets/css/style.css')}}" rel="stylesheet" type="text/css" />
