<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Laravel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A premium admin dashboard template by Mannatthemes" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('metrica.layouts.header-script')
    </head>
    <body class="dark-topbar">
        @include('metrica.layouts.head-navbar')
        <div class="page-wrapper">
            @include('metrica.layouts.side-navbar')
            <div class="page-content">
                <div class="container-fluid">
                    @yield('content')
                </div>
                <footer class="footer text-center text-sm-left">
                    &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
                </footer><!--end footer-->
            </div>
            @include('metrica.layouts.footer-script')
        </div>
    </body>
</html>