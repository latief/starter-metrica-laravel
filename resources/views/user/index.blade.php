@extends('metrica.layouts.master')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')

<!-- DataTables -->
<link href="{{ URL::asset('metrica-assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('metrica-assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ URL::asset('metrica-assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- sweet alert -->
<link href="{{ URL::asset('metrica-assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">

@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h4 class="page-title">User</h4>
        </div><!--end page-title-box-->
    </div><!--end col-->
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive dash-social">
					<table id="user-datatable" class="table">
						<thead class="thead-light">
				            	<tr>
				                	<th>Name</th>
				                	<th>Email</th>
				            	</tr>
				            </thead>
					</table>
				</div>		
			</div>
		</div>
		@if($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $error }}
                </div><br/>
            @endforeach
        @endif
        @if (session('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('info') }}
            </div>
        @endif
	</div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{ URL::asset('metrica-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('metrica-assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ URL::asset('metrica-assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('metrica-assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<!-- sweet alert -->
<script src="{{ URL::asset('metrica-assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function(e) {

		// ajax set-up
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // datatable
        var vacancyDatatable = $('#user-datatable').DataTable({
	        "processing": true,
	        "serverSide": true,
	        "ajax": '{{ $urlDatatable }}',
	        "columns": [
	            {
	                data: 'name'
	            },
	            {
	                data: 'email'
	            }
	        ]
	    });
	});
</script>
@endsection